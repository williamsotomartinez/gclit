'''
GCLDT
Guadeloupean Creole Language Identificcation Tool

fasttext wrapper for Guadeloupean Creol (gcf) identification.
'''

import sys
import getopt
import fasttext


class identifier:
    '''
    Object that stores a fasttext model that helps decide if an input text is
    written on Guadeloupean Creole or not.
    '''

    def __init__(self, model='gcf32dim.bin', ):
        '''
        Load the gcf fasttext model.

        INPUT
            model: path to the gcf language identification model.
        '''

        self.model = fasttext.load_model(model)

    def __preprocess(self, text):
        '''
        Preprocess a text input. Normlize dashes and apostrophes, remove
        unwanted characters and reduces white spaces.

        INPUT:
            text: string with the text to be preprocessed.

        OUTPUT: preprocessed text.
        '''

        text = text.lower()
        text = text.replace('´', '\'')
        text = text.replace('’', '\'')
        text = text.replace('–', '-')
        text = text.replace('—', '-')
        remove = '.,;:?!()[]{}<>«»"“”/\\=+'
        text = ''.join(list(filter(lambda c: c not in remove, text)))
        text = text.replace('\t', ' ')
        text = text.replace('\r', ' ')
        text = text.replace('\n', ' ')
        while '  ' in text:
            text = text.replace('  ', ' ')
        return text

    def get_probability(self, text):
        '''
        Calculate the probability that an input text is written on
        Guadeloupean Creole.

        INPUT:
            text: string with the text to be preprocessed.

        OUTPUT: probability of the input text being Guadeloupean Creole.
        '''

        # Preprocess the input text
        text = self.__preprocess(text)

        # Predict the probability of all the available lanugages
        prediction = self.model.predict(text, k=105)

        # Get the position of Guadeloupean Creole
        index = prediction[0].index('__label__gcf')

        # Return the probabilty of Guadeloupean Creole
        return prediction[1][index]

    def is_gcf(self, text, criteria='position', threshold=0.90):
        '''
        Decide if a text is on Guadeloupean Creole either because it is the
        most liekly language or because its probabilty is above a certain
        threshold.

        INPUT:
            text: string with the text to be preprocessed.
            criteria: 'position' to classify an input text as Guadeloupean
                      Creole when it is the most likely language predicted by
                      the model.
                      'threshold' to classify an input text as Guadeloupean
                      Creole when its probability of being so is above a
                      certain value.
            threshold: when using the threshold criteria, minimum probability
                       to classify a text as Guadeloupean Creole.

        OUTPUT: boolean indicating if the input text is Guadeloupean Creole.
        '''

        # If using the 'position' crtieria
        if criteria == 'position':

            # Preprocess the input text
            text = self.__preprocess(text)

            # If the most likely language is Guadeloupean Creole
            if self.model.predict(text)[0][0] == '__label__gcf':

                # Return True
                return True

            # In any other case
            else:

                # Return False
                return False

        # If using the 'threshold' crtieria
        elif criteria == 'threshold':

            # Calculate the probability of Guadeloupean Creole
            probability = self.get_probability(text)

            # If the probability is at least equal to the threshold
            if probability >= threshold:

                # Return True
                return True

            # In any other case
            else:

                # Return False
                return False

        # In any other case
        else:

            # Let the user know about the invalid criteria
            raise ValueError("ValueError: invalid criteria")

    def predict(self, text, k=1):
        '''
        Given a value k, use the gcf language identification model to predict the
        k most likely languages of an input text.

        INPUT:
            text: string with the text to be preprocessed.
            k: number of most likely languages to predict.

        OUTPUT: a nested touple with the predicted labels in the position 0
                and ther respective probability in the position 1
        '''

        # Preprocess the input text
        text = self.__preprocess(text)

        # Return the predicted most likely languages
        return self.model.predict(text, k=k)


def main(argv):
    '''
    Command line interface
    '''

    # Capture options and arguments from the command line
    try:
        opts, args = getopt.getopt(argv, "hm:a:c:t:k:s:i:o:", [
            'help',
            'model=',
            'action=',
            'criteria=',
            'threshold=',
            'k=',
            'string=',
            'ifile=',
            'ofile='
        ])
    except getopt.GetoptError:
        sys.exit(2)

    # Initialize command line variables
    printhelp = False
    model = ''
    action = ''
    criteria = ''
    threshold = ''
    k = ''
    string = ''
    ifile = ''
    ofile = ''

    # Assign the right value to the command line variables
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            printhelp = True
        elif opt in ("-m", "--model"):
            model = arg
        elif opt in ("-a", "--action"):
            action = arg
        elif opt in ("-c", "--criteria"):
            criteria = arg
        elif opt in ("-t", "--threshold"):
            threshold = float(arg)
        elif opt in ("-k", "--k"):
            k = int(arg)
        elif opt in ("-s", "--string"):
            string = arg
        elif opt in ("-i", "--ifile"):
            ifile = arg
        elif opt in ("-o", "--ofile"):
            ofile = arg

    if printhelp:
        print('-h, --help: help')
        print('-m, --model: gcf language identification model')
        print('-a, --action: action to execute')
        print('   get_probability: get the probability of being gcf')
        print('   is_gcf: determine if is gcf or not')
        print('   predict: predict most likely languages')
        print('-c, --criteria: criteria for is_gcf')
        print('   position: if gcf is the most likely language')
        print('   threshold: if the gcf probability is above the thershold')
        print('-t, --threshold: value for the is_gcf threshold')
        print('-k, --k: number of language for predict')
        print('-s, --string: inline string to detect gcf')
        print('-i, --ifile: input file with text to detect gcf')
        print('-o, --ofile: output file to store the results')

    if model:
        temp_identifier = identifier(model)
    else:
        temp_identifier = identifier()

    if not string and not ifile:
        sys.exit()
    elif ifile:
        with open(ifile, 'r', encoding='utf-8') as file:
            inputText = file.read()
        inputText = inputText.split('\n')
    elif string:
        inputText = [string]

    if ofile:
        outputText = ''

    if not action:
        sys.exit()
    elif action == 'get_probability':
        for line in inputText:
            probability = temp_identifier.get_probability(line)
            if ofile:
                outputText += str(probability)+'\n'
            else:
                print(probability)
    elif action == 'is_gcf':
        for line in inputText:
            if not criteria:
                gcf = temp_identifier.is_gcf(line)
            elif criteria == 'position':
                gcf = temp_identifier.is_gcf(line, criteria=criteria)
            elif criteria == 'threshold':
                if not threshold:
                    gcf = temp_identifier.is_gcf(line, criteria=criteria)
                else:
                    gcf = temp_identifier.is_gcf(line, criteria=criteria,
                                               threshold=threshold)
            if ofile:
                outputText += str(gcf)+'\n'
            else:
                print(gcf)
    elif action == 'predict':
        for line in inputText:
            if not k:
                prediction = temp_identifier.predict(line)
            else:
                prediction = temp_identifier.predict(line, k=k)
            if ofile:
                outputText += str(prediction)+'\n'
            else:
                print(prediction)

    if ofile:
        with open(ofile, 'w', encoding='utf-8') as file:
            file.write(outputText)


# If thecode is executed from the command line
if __name__ == '__main__':

    # Run the command line interface
    main(sys.argv[1:])
