# Guadeloupean Creole Language Identification Tool

This is the resulting wrapper for the experiments of the paper [Language Identification of Guadeloupean Creole](https://hal.archives-ouvertes.fr/hal-03047144/)

## Files description

- **`gclit.py`** contains the last version of the `Guadeloupean Creole Language Identification Tool`,  a `fastText` wrapper for Guadeloupean Creole (gcf) identification.
- **`gcf16dim.bin`** contains a 16 dimensions `FastText` model trained for language identification with plenty of Guadeloupean Creole examples.
- **`gcf32dim.bin`** contains a 32 dimensions `FastText` model trained for language identification with plenty of Guadeloupean Creole examples.
- **`gcf64dim.bin`** contains a 64 dimensions `FastText` model trained for language identification with plenty of Guadeloupean Creole examples.

## Requirements

`gclit` requires [Python](https://www.python.org/) (version ≥ 3.4) and [FastText](https://fasttext.cc/) (version ≥ 0.9) as well as its respective dependencies.

## Installation

To use the wrapper you need to have `gclit.py` and one of the .bin language model files on your working directory.

## Use

### On Python

#### Getting started

To use the wrapper from a python project you first need to import the `identifier` class and instantiate it, like this:

```
>>> from gclit import identifier
>>> myidentifier = identifier(model = 'gcf32dim.model')
```

The `model` argument is optional. If it is not provided the wrapper will look for the last published language model in the current directory.

Once the `identifier` object has been created you can start using it.

#### Calculate Probability

If you want to calculate the probability that a given text is Guadeloupean Creole you can use the `get_probability` method of the `identifier`, like this:

```
>>> myidentifier.get_probability('Plita, mwen ké on chanpyon ténis !')
0.9999831914901733
```

#### Decide if it is Guadeloupean Creole

If you want the detector to tell you if a given text is Guadeloupean Creole or not you can use the `is_gcf` method of the `identifier`, like this:

```
>>> myidentifier.is_gcf('Plita, mwen ké on chanpyon ténis !', criteria='position')
True
```

The `criteria` argument is optional. By default it is set to `position` which will classify a text as Guadeloupean Creole if that is the most likely language acording to the model, regardles of the probability.

The `criteria` can be changed to `threshold` in which case the decision will be made if the probability of being Guadeloupean Creole is above a certain threshold, like this:

```
>>> myidentifier.is_gcf('Plita, mwen ké on chanpyon ténis !', criteria='threshold', threshold=0.99)
True
```

The `threshold` argument is optional and indicates the minimum probability threshold that a text requiresto be accepted as Guadeloupea Creole. By default it is set to 0.9.

#### Most likely languages

If you want to get the most likely languages of a given text and their probability you can use the `predict` method of the `identifier`, like this:

```
>>> myidentifier.predict('Plita, mwen ké on chanpyon ténis !', k=2)
(('__label__gcf', '__label__est'), array([9.99983191e-01, 2.10275884e-05]))
```

The `k` argument is optional and indicates how many of the most likely languages you want. By default it is set to 1.

### On command line

To use the wrapper from the command line all you need to do is call the file with the appropriate parameters, like this:

```
user@domain:~$ python gclit.py -a get_probability -s "Plita, mwen ké on chanpyon ténis !"
0.9999831914901733
```

The options accepted by the program are:

- -h, -\-help: display a small help prompt.
- -m, -\-model: used to indicate a path to a language model.
- -a, -\-action: used to indicate the action to perform (get\_probability, is\_gcf or predict).
- -c, -\-criteria: used to indicate the criteria when performing the is\_gcf action (position or threshold).
- -t, -\-threshold: used to indicate the threshold when performing the is\_gcf action with the threshold criteria.
- -k, -\-k: used to indicate the number of languages when performing the predict action.
- -s, -\-string: used to indicate an in line string to be evaluated.
- -i, -\-ifile: used to indicate a path to an input file where each line contains a different string to be evaluated
- -o, -\-ofile: used to indicate a path to an output file to store the obtained results.

If both an input file and an in line string are provided the input file will be the one processed.
If no output file is provided the output will be displayed on the command line

## Citation

```
@inproceedings{soto:hal-03047144,
	TITLE = {{Language Identification of Guadeloupean Creole}},
	AUTHOR = {Soto, William},
	URL = {https://hal.archives-ouvertes.fr/hal-03047144},
	BOOKTITLE = {{2{\`e}mes journ{\'e}es scientifiques du Groupement de Recherche Linguistique Informatique Formelle et de Terrain (LIFT)}},
	ADDRESS = {Montrouge, France},
	EDITOR = {Poibeau, Thierry and Parmentier, Yannick and Schang, Emmanuel},
	PUBLISHER = {{CNRS}},
	PAGES = {54-59},
	YEAR = {2020},
	KEYWORDS = {Language Identification ; Machine Learning ; Guadeloupean Creole},
	HAL_ID = {hal-03047144},
	HAL_VERSION = {v1},
}
```